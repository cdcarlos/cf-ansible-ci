"""Generating CloudFormation template."""
import requests
from troposphere import (
    Base64,
    ec2,
    GetAtt,
    Join,
    Output,
    Parameter,
    Ref,
    Template,
)

from troposphere.iam import (
    InstanceProfile,
    PolicyType as IAMPolicy,
    Role,
)

from awacs.aws import (
    Action,
    Allow,
    Policy,
    Principal,
    Statement,
)
from awacs.sts import AssumeRole

ApplicationName = "jenkins"
ApplicationPort = "8080"

GithubAnsibleURL = "https://bitbucket.org/cdcarlos/cf-ansible-ci.git"
AnsiblePullCmd = f"/usr/local/bin/ansible-pull -U {GithubAnsibleURL} {ApplicationName}.yml -i localhost"

r = requests.get("https://api.ipify.org")
PublicCidrIp = r.text

t = Template()

t.set_description("Jenkins Server")

t.add_parameter(
    Parameter(
        "KeyPair",
        Description="Name of an existing EC2 KeyPair to SSH",
        Type="AWS::EC2::KeyPair::KeyName",
        ConstraintDescription="must be the name of an existing EC2 KeyPair.",
    ))

t.add_resource(
    ec2.SecurityGroup(
        "SecurityGroup",
        GroupDescription=f"Allow SSH and TCP/{ApplicationPort} access",
        SecurityGroupIngress=[
            ec2.SecurityGroupRule(
                IpProtocol="tcp",
                FromPort="22",
                ToPort="22",
                CidrIp=f"{PublicCidrIp}/0",
            ),
            ec2.SecurityGroupRule(
                IpProtocol="tcp",
                FromPort=ApplicationPort,
                ToPort=ApplicationPort,
                CidrIp="0.0.0.0/0",
            ),
        ],
    ))

t.add_resource(
    Role("Role",
         AssumeRolePolicyDocument=Policy(Statement=[
             Statement(Effect=Allow,
                       Action=[AssumeRole],
                       Principal=Principal("Service", ["ec2.amazonaws.com"]))
         ])))

t.add_resource(
    InstanceProfile("InstanceProfile", Path="/", Roles=[Ref("Role")]))

ud = Base64(
    Join("\n", [
        "#!/bin/bash", "sudo yum install -y git python3",
        "sudo pip3 install ansible", AnsiblePullCmd,
        f"sudo echo '*/10 * * * * {AnsiblePullCmd}' > /etc/cron.d/ansible-pull"
    ]))

t.add_resource(
    ec2.Instance(
        "jenkins",
        ImageId="ami-07c8bc5c1ce9598c3",
        InstanceType="t2.micro",
        SecurityGroups=[Ref("SecurityGroup")],
        KeyName=Ref("KeyPair"),
        UserData=ud,
        IamInstanceProfile=Ref("InstanceProfile"),
    ))

t.add_output(
    Output(
        "InstancePublicIp",
        Description="Public IP of our instance.",
        Value=GetAtt("jenkins", "PublicIp"),
    ))

t.add_output(
    Output(
        "WebUrl",
        Description="Application endpoint",
        Value=Join("", [
            "http://",
            GetAtt("jenkins", "PublicDnsName"), ":", ApplicationPort
        ]),
    ))

print(t.to_json())
