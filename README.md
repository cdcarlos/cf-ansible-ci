# CloudFormation and Ansible CI

![Build Status](https://codebuild.us-east-2.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoiK09NcHF6OTNucFpnRkpXOXkvSjRRSVQ0VVZsK0xXWTlHeGNxRkpyZnFULzFLNkdEWTBKNUQrNEJKbGx6WGE3S1RrV2FnUXZlV1pMSFJDcHJ2YklBTlI0PSIsIml2UGFyYW1ldGVyU3BlYyI6InZSa2kxUXp3QjNGNU1lNHEiLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master)

# Requirements

-   Python 3.8
-   AWS CLI v2
-   AWS CLI Profile

# Environment Setup

## Virtual env

Create virtual env by running the following command:

```bash
$ python -m venv .venv-cfans-ci
```

Once your virtual environment has been created, activate it with:

```bash
$ source .venv-cfans-ci/bin/activate
```

## AWS SSH Key

```bash
$ aws ec2 create-key-pair --key-name EffectiveDevOpsAWS --query 'KeyMaterial' --output text  --profile cc > ~/.ssh/EffectiveDevOpsAWS.pem
$ chmod 400 ~/.ssh/EffectiveDevOpsAWS.pem
```

## Install Dependencies

### Development Environment

Install development environment dependencies:

```bash
$ pip install -r requirements-env.txt
```

### Application Dependencies

```bash
$ pip install -r requirements.txt
```

# Usage

## Jenkins

### Generate Jenkins CloudFormation Template

```bash
$ python jenkins-cf-template.py > jenkins-cf.template
```

### Deploy Jenkins Stack

```bash
$ aws cloudformation create-stack --capabilities CAPABILITY_IAM --stack-name jenkins --template-body file://jenkins-cf.template --parameters ParameterKey=KeyPair,ParameterValue=EffectiveDevOpsAWS --profile cc
```

StackId: "arn:aws:cloudformation:us-east-2:995360066764:stack/ansible/239225d0-e811-11ea-ad4b-0a86fd141a00"

#### Wait for Stack Completion

```bash
$ aws cloudformation wait stack-create-complete --stack-name jenkins --profile cc
```

#### Get Jenkins Public IP

```bash
$ aws cloudformation describe-stacks --stack-name jenkins --query 'Stacks[0].Outputs[0]' --profile cc
```

Output:

```json
{
    "OutputKey": "InstancePublicIp",
    "OutputValue": "3.23.89.177",
    "Description": "Public IP of our instance."
}
```

#### Change Default Password

Once Jenkins is up and running, you can get the default password:

```bash
$ ssh -i ~/.ssh/EffectiveDevOpsAWS.pem ec2-user@3.23.89.177 sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```

Open your browser and change the default password: `http://{JENKINS_IP}:8080`

# Clean Up

## Delete CloudFormation Stack

```bash
$ aws cloudformation delete-stack --stack-name jenkins --profile cc
```
